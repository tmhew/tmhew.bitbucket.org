sap.ui.define([
    "sap/ui/core/UIComponent"
], function (UIComponent) {
    "use strict";

    var CustomComponent = UIComponent.extend("com.tm.Component", {
        metadata: {
            manifest: "json"
        }
    });

    return CustomComponent;
});
