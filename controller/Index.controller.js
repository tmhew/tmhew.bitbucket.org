sap.ui.define([
    "sap/ui/core/mvc/Controller"
], function (Controller) {
    "use strict";

    var CustomController = Controller.extend("com.tm.controller.Index");

    return CustomController;
});
