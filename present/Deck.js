sap.ui.define([
    "sap/ui/core/Control"
], function (Control) {
    "use strict";

    var CustomControl = Control.extend("com.tm.ui5slide.Deck");

    return CustomControl;
});
