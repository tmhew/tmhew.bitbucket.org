sap.ui.define([
], function () {
    "use strict";

    sap.ui.getCore().initLibrary({
        name: "com.tm.present",
        version: "1.0.0",
        dependencies: [
            "sap.ui.core"
        ],
        types: [],
        interfaces: [],
        controls: [],
        elements: []
    });

    return com.tm.present;
});
